


var playing = false;
var score;
var action;
var timereamaning;
var correctAnswer;
// if we click on the start/reset 

document.getElementById("startreset").onclick = function(){
    

    // if we are playing
    if(playing == true){
        location.reload();
        
    }else{  //if we are not playing
        
        //change mode playing
        playing = true;
        
        //set score to 0
        score = 0;
        document.getElementById("scorevalue").innerHTML = score;
        
        //show countdown box
        show("timeremaining"); 
        timereamaning = 60;
        document.getElementById("timeremainingvalue").innerHTML = timereamaning;
        
        
        //hige gameover box
        hide("gameover");
        
        
        
        //change button to reset
        document.getElementById("startreset").innerHTML = "Reset Game";
        
        //start coundown
        
        startCountdown();
        
        generateQA();
    }
}


//Clickingonanswer box

for(i=1; i<5; i++){
    
   document.getElementById("box" + i).onclick=function(){
    
    
//checking if we are playing
    if(playing==true){
        if(this.innerHTML == correctAnswer){
            //corect answer
            score++;
            document.getElementById("scorevalue").innerHTML = score;
            
            //hide wrong box and show correct
            
            hide("wrong");
            show("correct");
            setTimeout(function(){
                hide("correct")
            }, 1000);
            
            //Generate New Question
            
            generateQA();
            
            
            
        }else{
            //wrong answer
             hide("correct");
            show("wrong");
            setTimeout(function(){
                hide("wrong")
            }, 1000);
        }
    }
    
} 
}






//functions
//functions
//functions
//functions



//Start counter
function startCountdown(){
    
    action = setInterval(function(){
        timereamaning -= 1;
    document.getElementById("timeremainingvalue").innerHTML = timereamaning;
        
        //gameOver
        if(timereamaning == 0){
        stopCountdown();
        show("gameover");
            
        document.getElementById("gameover").innerHTML = "<p>Game over!</p><p>Your score is " + score + ".</p>";
        
        hide("timeremaining");
        hide("correct");
        hide("wrong");
        playing = false;
            
        document.getElementById("startreset").innerHTML = "Start Game";
        
        }
    }, 1000);
}



//Stop Counter
function stopCountdown(){
    clearInterval(action);
}

//Hide element
function hide(Id){
    document.getElementById(Id).style.display = "none";
}

//Show element
function show(Id){
    document.getElementById(Id).style.display = "block";
}


//generate question and answer

function generateQA(){
    
    var x = 1 + Math.round(9*Math.random());
    var y = 1 + Math.round(9*Math.random());
    
    correctAnswer = x * y;
    document.getElementById("question").innerHTML = x + "x" + y;
    
    
    // Fill One Box With Correct Answer
    var correctPosition = 1 + Math.round(3*Math.random());
    
    document.getElementById("box"+correctPosition).innerHTML = correctAnswer;
    
    
    // Fill them with wrong answer
    
    var answers = [correctAnswer];
    
    for(i=1; i<5; i++){
        if(i != correctPosition){
            var wrongAnswer;
            
            do{
                wrongAnswer = (1 + Math.round(9*Math.random()))*(1 + Math.round(9*Math.random()));
            }while(answers.indexOf(wrongAnswer)>-1);
            
                
        document.getElementById("box" + i).innerHTML = wrongAnswer;
            
        answers.push(wrongAnswer);
            
        }
        
    }
    
}
















