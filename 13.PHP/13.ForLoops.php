<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,
initial-scale=1">
    <title>
        For Loops
    </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style>

		h1{
			color: blue;
		}
		
		
    </style>
</head>


<body>

    <div class="container-fluid">

            <h1>For Loops</h1>
		
		<div>
<?php
	
			for($i = 1; $i <=10; $i++){
				echo $i . "<br/>";
			}
		
			
			$carmakes = array("BMW","Audi","Mercedes");
			
			foreach($carmakes as $value){
				
				echo $value . "<br/>";
			}
			
			
			$shoopingBasket = array("a"=>"yogurt","b"=>"milk","c"=>"bread");
			
			foreach($shoopingBasket as $key=>$value){
				
				echo $key . " : " . $value . "<br/>";
			}
			
			
?>
		
		</div>
			
		
        
    </div>


    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/j
query.min.js">
    </script>
    <script src="js/bootstrap.min.js">
        
    </script>
   
        
    
    
    
</body>

</html>