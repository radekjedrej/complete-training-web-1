<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,
initial-scale=1">
    <title>
        File Handling
    </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style>

		cialo{
			padding-bottom: 50px;
		}
		
		
		.containingDiv{
			
			border: 1px solid purple;
			margin-top: 100px;
			border-radius: 25px;
			
		}
		
		h3{
			color: #00b5b5;
			
		}
		
		.naglowek{
			color: purple;
		}
		
		.przystawka{
			color: #00d1c9
		}
		
    </style>
</head>


<body >

<div>	
<?php

	include "1.header.php";
	
?>
</div>
	
    <div class="container-fluid">

         <div class="row">
			<div class="col-sm-offset-1 col-sm-10 containingDiv">
			 	<h1 class="naglowek">File Handling</h1>
				<div class="cialo">
					
				
					
					<h3 class="przystawka">fopen/fclose: </h3>
					
					
<?php

	if(file_exists("sometextyyyy.txt")){
		$myFile = fopen("sometext.txt", "r");
		fclose($myFile);
	}else{
		echo "<p>This file does not exist</p>";
		
	}			
			
								
?>
					
					
					<h3 class="przystawka">fread: </h3>
					
					
<?php

					
	$fileHandle = fopen("sometext.txt", "r") or die("Unable To Open File");				
	$fileContent = fread($fileHandle, filesize("sometext.txt"));		fclose($fileHandle);		
	var_dump($fileContent);
					
				
?>
					
					<h3 class="przystawka">file_get_contents:</h3>
					
<?php				

	$fileContent = file_get_contents("sometext.txt") or die("Unable to read file");
	var_dump($fileContent);
					
					
					
?>					
					
					<h3 class="przystawka">file function: </h3>
					
<?php

	$fileContent = file("sometext.txt") or die("Unable to read file");
	var_dump($fileContent);
		echo "</br>";
		echo "</br>";
	foreach($fileContent as $line){
		echo $line . "</br>";
	}				
					
?>					
					<h3 class="przystawka">fwrite: </h3>
					
<?php

	$fileHandle = fopen("sometext.txt", "w") or die("Unable to open file");				
	fwrite($fileHandle, "This is new Content") or die("Unable to write");
	$fileContent = file("sometext.txt") or die("Unable to read file");
	var_dump($fileContent);
					
?>
					
					<h3 class="przystawka">file_put_contents: </h3>
					
<?php

	file_put_contents("sometext.txt", "\r this is some other new content!", FILE_APPEND) or die("Unable to write");
	$fileContent = file("sometext.txt") or die("Unable to read file");
	var_dump($fileContent);
					
?>
				
				
		
			 
			 </div>
		</div>   
		
		
			
		
        
    </div>
		
	</div>	
<?php

	include "1.footer.php";
	
?>

	  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/j
query.min.js">
    </script>
    <script src="js/bootstrap.min.js">
        
    </script>
   
        
    
    
    
</body>

</html>