<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,
initial-scale=1">
    <title>
        Filter User Input
    </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style>

		h1{
			color: blue;
		}
		
		h3{
			color: #006776;
		}
		
    </style>
</head>


<body>

    <div class="container-fluid">

            <h1>Filter User Input</h1>
			<h3>Clean user inputs: </h3>
		
		<div>
<?php
	
			//Username example
			$myUsername = '<script>window.alert("Hi)</script>';
			
			$myUsername = filter_var($myUsername, FILTER_SANITIZE_STRING);
			
			
			echo $myUsername;
			
			
			//email examle
			echo '</br>';
			$myEmail = 'radek@    gmail  robot\\\ .co.uk';
			$myEmail = filter_var($myEmail, FILTER_SANITIZE_EMAIL);
			
			echo $myEmail;
			
			//URL example
			echo '</br>';
			$myURL = "http:// www. google.com";
			
			$myURL = filter_var($myURL, FILTER_SANITIZE_URL);
			
			echo "$myURL";
			
			
?>
		
			<div>
			<h3>Validate User Input:</h3>
			</div>
			
<?php
			
			
	//Email Validation	
			
			$myEmail = 'radek@    gmail  robot\\\ .co.uk';
			$myEmail = filter_var($myEmail, FILTER_SANITIZE_EMAIL);
			echo "<p>Cleaned Email: $myEmail </p>";
			echo "<p>Email validation: " . filter_var($myEmail, FILTER_VALIDATE_EMAIL) . "</p>";
			
			if(filter_var($myEmail, FILTER_VALIDATE_EMAIL)){
				
				echo "<p>Valid Email</p>";
			}else{
				
				echo "<p>Invalid Email</p>";
			}
			
	// URL Validation
			
			
			echo '</br>';
			$myURL = "http:// www. google.com";
			
			$myURL = filter_var($myURL, FILTER_SANITIZE_URL);
			
			echo "<p>Cleaned URL: $myURL</p>";
			echo "<p>URL validation: " . filter_var($myURL, FILTER_VALIDATE_URL) . "</p>";
			
			
?>			
			
		</div>
			
		
        
    </div>


    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/j
query.min.js">
    </script>
    <script src="js/bootstrap.min.js">
        
    </script>
   
        
    
    
    
</body>

</html>