<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,
initial-scale=1">
    <title>
       PHP Session:
    </title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style>

		h1{
			color: purple;
		}
		
		h3{
			color: #00b5b5;
			
		}
		.contactForm{
			
			border: 1px solid purple;
			margin-top: 100px;
			border-radius: 15px;
			padding-bottom: 30px;
			
		}
		
		.guziczek{
			
			margin-bottom: 30px;
		}
		
		
    </style>
</head>


<body>

    <div class="container-fluid">

         <div class="row">
			<div class="col-sm-offset-1 col-sm-10 contactForm">
			 	<h1>PHP Session:</h1>

				<h3>Example 1:</h3>
<?php

	// start session
				
				session_start();
				
				$_SESSION["firstname"] = "Mark";
				$_SESSION["lastname"] = "Zuckerberg";
				
				$firstname = $_SESSION["firstname"];
				$lastname = $_SESSION["lastname"];
				
				echo "Hi! $firstname $lastname";

				if(isset($_SESSION["firstname"])){
					unset($_SESSION["firstname"]);
				}
				
				echo $_SESSION["firstname"]? 1:0;
				
	// destroy session
				
				session_destroy();
				
?>
				
		
				
		</div>   
		
		
			
		
        
    </div>


    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/j
query.min.js">
    </script>
    <script src="js/bootstrap.min.js">
        
    </script>
   
        
    
    
    
</body>

</html>